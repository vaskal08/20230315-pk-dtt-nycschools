//
//  Subway.swift
//  NYCschools
//
//  Created by Kamb, Peter on 3/15/23.
//

import Foundation
import UIKit
import SwiftUI

enum NYCSubwayService: String, Identifiable {
    
    // Handles the main simple MTA subway services
    // Does not yet handle some of the more complicated services listed,
    // such as "5 to Franklin Ave" or "Q to Prospect Park"
    
    // Digits 0-9 allow for displaying SAT scores as subway service bullets
    
    var id: String {
        // TODO: Identifiable enum causes issues with SwiftUI ForEach.
        // > the ID 'three' occurs multiple times within the collection, this will give undefined results!
        // Investigate using a unique enum `id` other than the enum's raw value
        rawValue
    }
    
    case A, C, E
    case B, D, F, M
    case G
    case L
    case J, Z
    case N, Q, R, W
    case one = "1", two = "2", three = "3"
    case four = "4", five = "5", six = "6"
    case seven = "7"
    case T
    case S
    
    // Historic and internal
    case zero  = "0"
    case eight = "8"
    case nine  = "9"
    
    var color: Color { Color(uiColor) }
    
    var uiColor: UIColor {
        switch self {
        case .A, .C, .E:         return #colorLiteral(red: 0, green: 0.2235294118, blue: 0.6509803922, alpha: 1)
        case .B, .D, .F, .M:     return #colorLiteral(red: 1, green: 0.3882352941, blue: 0.09803921569, alpha: 1)
        case .G:                 return #colorLiteral(red: 0.4235294118, green: 0.7450980392, blue: 0.2705882353, alpha: 1)
        case .L:                 return #colorLiteral(red: 0.6549019608, green: 0.662745098, blue: 0.6745098039, alpha: 1)
        case .J, .Z:             return #colorLiteral(red: 0.6, green: 0.4, blue: 0.2, alpha: 1)
        case .N, .Q, .R, .W:     return #colorLiteral(red: 0.9882352941, green: 0.8, blue: 0.03921568627, alpha: 1)
        case .one, .two, .three, .nine: return #colorLiteral(red: 0.9333333333, green: 0.2078431373, blue: 0.1803921569, alpha: 1)
        case .four, .five, .six: return #colorLiteral(red: 0, green: 0.5764705882, blue: 0.2352941176, alpha: 1)
        case .seven:             return #colorLiteral(red: 0.7254901961, green: 0.2, blue: 0.6784313725, alpha: 1)
        case .T:                 return #colorLiteral(red: 0, green: 0.6784313725, blue: 0.8156862745, alpha: 1)
        case .S, .zero:          return #colorLiteral(red: 0.5019607843, green: 0.5058823529, blue: 0.5137254902, alpha: 1)
        case .eight:             return #colorLiteral(red: 0.3019607843, green: 0.6666666667, blue: 0.8, alpha: 1)
        }
    }
    
    var textColor: Color {
        switch self {
        case .N, .Q, .R, .W:
            return .black
        default:
            return .white
        }
    }
    
    var systemImageName: String { "\(rawValue.lowercased()).circle.fill" }
    
    var image: some View {
        Image(systemName: systemImageName)
            .foregroundStyle(textColor, color)
    }
    
}

extension String {
    
    // Service signs for each character that matches a subway service
    // Not every character has a subway service!
    // But digits 0-9 are handled
    var subwayServices: [NYCSubwayService] {
        self.compactMap({ NYCSubwayService(rawValue: String($0)) })
    }
}

