//
//  NYCschoolsApp.swift
//  NYCschools
//
//  Created by Kamb, Peter on 3/14/23.
//

import SwiftUI

@main
struct NYCschoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolsList()
        }
    }
}
