//
//  Font.swift
//  NYCschools
//
//  Created by Kamb, Peter on 3/15/23.
//

import SwiftUI

extension Font {
    
    static var subwaySignFont: Font {
        return Font.custom("Helvetica-bold", size: 24)
    }
    
}
