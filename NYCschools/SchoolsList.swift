//
//  SchoolsList.swift
//  NYCschools
//
//  Created by Kamb, Peter on 3/14/23.
//

import SwiftUI
import Combine

class SchoolsListViewModel: ObservableObject {
    
    var cancellable: AnyCancellable?
    
    @Published var schools: [School] = []
    @Published var didFetch: Bool = false
    
    func fetchSchools() {
        self.cancellable = NYCOpenDataWebService.fetchSchools(limit: 30)
            .mapError({ (error) -> Error in
                self.didFetch = true
                return error
            })
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in

            }, receiveValue: { apiSchools in
                let subwaySchools = apiSchools.filter({ !$0.subwayService.isEmpty })
                self.schools = subwaySchools
                self.didFetch = true
            })
    }
    
}

struct SchoolsList: View {
    
    @ObservedObject var viewModel: SchoolsListViewModel = SchoolsListViewModel()
    
    var body: some View {
        NavigationView {
            NavigationStack {
                List() {
                    if viewModel.schools.isEmpty {
                        // Use Times Station NQRWS service icons as a simple loading view.
                        HStack {
                            ForEach("NQRWS".subwayServices) { service in
                                service.image
                            }
                            Spacer()
                            progressOrError
                        }
                        .contentShape(Rectangle())
                        .onTapGesture {
                            self.viewModel.didFetch = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                // small delay so that we can actually see the spinner
                                viewModel.fetchSchools()
                            }
                        }
                    } else {
                        ForEach(viewModel.schools) { school in
                            NavigationLink(value: school) {
                                VStack(alignment: .leading) {
                                    Text(school.schoolName)
                                        .font(.subwaySignFont)
                                    HStack(spacing: 4) {
                                        ForEach(school.subwayService) {
                                            $0.image
                                                .font(.subwaySignFont)
                                        }
                                        Spacer()
                                    }
                                }
                            }
                        }
                    }
                }
                .navigationTitle("Subway Schools")
                .onAppear() {
                    viewModel.fetchSchools()
                }
                .navigationDestination(for: School.self) { school in
                    SchoolDetail(viewModel: SchoolDetailViewModel(school: school, satResults: nil))
                }
            }
        }
        .accentColor(.black)
        .font(.subwaySignFont)
        .onAppear() {
            viewModel.fetchSchools()
        }
    }
    
    @ViewBuilder var progressOrError: some View {
        if !viewModel.didFetch {
            ProgressView().progressViewStyle(CircularProgressViewStyle())
        } else {
            Image(systemName: "arrow.down.right.square").font(.subwaySignFont)
        }
    }
}

struct SchoolsList_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsList()
    }
}
