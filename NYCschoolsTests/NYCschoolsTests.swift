//
//  NYCschoolsTests.swift
//  NYCschoolsTests
//
//  Created by Kamb, Peter on 3/14/23.
//

import XCTest

final class NYCschoolsTests: XCTestCase {
    
    func testExample() throws {
        XCTAssert("0123456789".subwayServices.count == 10)
        XCTAssert("ACEBDFMGLJZNQRWTS".subwayServices.count == 17)
        XCTAssert(NYCSubwayService(rawValue: "A") == .A)
        XCTAssert(NYCSubwayService(rawValue: "a") != .A) // TODO: handle lowercase letters
        XCTAssertNil(NYCSubwayService(rawValue: "P")) // TODO: handle entire alphabet, not just active service
    }
    
}
